mkdir sopcast
cd sopcast

wget http://download.sopcast.com/download/sp-auth.tgz
tar xzvf sp-auth.tgz
sudo cp sp-sc-auth /usr/local/bin/
cd ..

wget http://www.sopcast.com/download/libstdcpp5.tgz
cd usr/lib/
sudo cp * /usr/local/lib/
sudo ldconfig
cd ../..

sudo apt-get install python-glade2 vlc
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/sopcast-player/sopcast-player-0.8.5.tar.gz
tar xzvf sopcast-player-0.8.5.tar.gz
cd sopcast-player
make
sudo make install

sopcast-player
